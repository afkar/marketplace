<form class="modal-content animate" style="border: none;" action="<?=base_url('index.php/dashboard/proses_login')?>" method="post">
			<div class="register-login-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="login-form">
                                <?php 
                                if($this->session->flashdata('pesan')!=null){
                                echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";}?>
								<h2>Login</h2>
									<div class="group-input">
										<label for="username">Username</label>
										<input type="text" name="username" style="padding: 12px 20px; border-radius: 10px;">
									</div>
									<div class="group-input">
										<label for="pass">Password *</label>
										<input type="password" name="password" style="padding: 12px 20px; border-radius: 10px;">
									</div>
									<div class="group-input gi-check">
										<div class="gi-more">
											<a href="<?=base_url('index.php/dashboard/reset')?>" class="forget-pass">Reset Password</a>
										</div>
									</div>
									<input type="submit" class="site-btn login-btn" style="border-radius: 10px;" name="login" value="Sign In">
								<div class="switch-login">
									<a href="<?=base_url('index.php/dashboard/register')?>" class="or-login">Or Create An Account</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>