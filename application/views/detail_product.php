<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="<?=base_url('index.php/dashboard')?>"><i class="fa fa-home"></i> Home</a>
                        <a href="<?=base_url('index.php/product')?>">Shop</a>
                        <span>Detail</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Product Shop Section Begin -->
    <section class="product-shop spad page-details">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <form action="<?=base_url('index.php/checkout/AddCart/'.$detailProduct->product_id)?>" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="product-pic-zoom">
                                    <img class="product-big-img" src="<?=base_url('asset/gambar_product/'.$detailProduct->image )?>" alt="">
                                    <div class="zoom-icon">
                                        <i class="fa fa-search-plus"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="product-details">
                                    <div class="pd-title">
                                        <h3><?= $detailProduct->product_name ?></h3>
                                    </div>
                                    <div class="pd-desc">
                                        <p><?= $detailProduct->description ?></p>
                                        <h4>Rp. <?= number_format($detailProduct->price) ?></h4>
                                    </div>
                                    <div class="pd-color">
                                        <h6>Color</h6>
                                        <div class="pd-color-choose">
                                            <div class="cc-item">
                                                <select name="color" class="form-control">
                                                    <?php foreach ($detailnya as $d): ?>
                                                        <option value="<?= $d->color_id ?>"><?= $d->color_name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                <br>
                                                <?php foreach ($detailnya as $d): ?>
                                                    <input type="radio" id="cc-black">
                                                    <label style="background: <?= $d->color_name ?>;" name="color1"></label>
                                                <?php endforeach ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pd-share">
                                        <div class="p-code" style="text-decoration: underline;">Stock</div>
                                        <br>
                                        <?php foreach ($detailnya as $d): ?>
                                            <div class="p-code"><?= $d->color_name ?> : <?= $d->stock ?></div>
                                            <br>
                                        <?php endforeach ?>
                                    </div>
                                    <br>
                                    <div class="quantity">
                                        <div class="pro-qty">
                                            <input type="text" value="1" name="qty">
                                        </div>
                                        <input type="submit" class="site-btn login-btn" value="Add to Cart">
                                    </div>
                                    <!-- <ul class="pd-tags">
                                        <li><span>Stok</span>: <?= $detailProduct->stock ?></li>
                                        <li><span>Dimension</span>: <?= $detailProduct->dimention ?></li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="product-tab">
                        <div class="tab-item">
                            <ul class="nav" role="tablist">
                                <li>
                                    <a class="active" data-toggle="tab" href="#tab-1" role="tab">DESCRIPTION</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab-2" role="tab">SPECIFICATIONS</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-item-content">
                            <div class="tab-content">
                                <div class="tab-pane fade-in active" id="tab-1" role="tabpanel">
                                    <div class="product-content">
                                        <div class="row">
                                            <div class="col-lg-7">
                                                <?= $detailProduct->description ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab-2" role="tabpanel">
                                    <div class="specification-table">
                                        <table>
                                            <tr>
                                                <td class="p-catagory">Price</td>
                                                <td>
                                                    <div class="p-price">Rp. <?= number_format($detailProduct->price) ?></div>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td class="p-catagory">Availability</td>
                                                <td>
                                                    <div class="p-stock"><?= number_format($detailProduct->stock) ?></div>
                                                </td>
                                            </tr> -->
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Shop Section End -->