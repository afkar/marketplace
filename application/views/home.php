<section class="hero-section">
	<div class="hero-items owl-carousel">
		<div class="single-hero-items set-bg" data-setbg="<?=base_url('asset/img/banner.png' )?>" style="background-position: right center; background-size: cover;">
			<div class="container">
				<div class="row">
				<h3 style="text-align: end; font-weight: bold; color: white; padding: 10px;">MAKE YOUR <br> OWN <br><p style="color: red; font-size: 30px; font-weight: bold; margin:0;">DESK</p></h3>
					<!-- <div class="col-lg-5">
						<h1><?= $product->product_name ?></h1>
						<?php if($product->product_id == "1"){
							echo "<p>Doll House Corp merupakan produsen mainan edukatif yang memproduksi Puzzle House. Puzzle House merupakan sebuah mainan edukatif untuk anak berusia 4-7 tahun yang berbentuk layaknya seperti rumah dan perabotannya umumnya.</p>";
						}else{
							echo "<p>Puzzle House memiliki panjang 45cm, lebar 25 cm, dan tinggi 35cm. Puzzle akan dijual dimulai dengan harga Rp 250.000 - Rp 325.000.</p>";
						}?>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Banner Section Begin -->
<div class="banner-section spad">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4">
				<div class="single-banner">
					<img src="<?=base_url('asset/gambar_product/home.jpeg' )?>" alt="">
					<!-- <div class="inner-text">
						<h4>Men’s</h4>
					</div> -->
				</div>
			</div>
			<div class="col-lg-4">
				<div class="single-banner">
					<img src="<?=base_url('asset/gambar_product/home.jpeg' )?>" alt="">
					<!-- <div class="inner-text">
						<h4>Women’s</h4>
					</div> -->
				</div>
			</div>
			<div class="col-lg-4">
				<div class="single-banner">
					<img src="<?=base_url('asset/gambar_product/home.jpeg' )?>" alt="">
					<!-- <div class="inner-text">
						<h4>Kid’s</h4>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner Section End -->