<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $judul ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <?=$this->session->flashdata('pesan');?>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered datatable" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Order Id</th>
            <th>Account Name</th>
            <th>Payment</th>
            <th>Ammount</th>
            <th>Qty</th>
            <th>Order Date</th>
            <th>Status Id</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($dataOrder as $order): ?>
            <tr>
                <td><?=$order->order_id?></td>
                <td><?=$order->fullname?></td>
                <td><?=$order->payment_name?></td>
                <td><?=number_format($order->amount)?></td>
                <td><?=$order->output?></td>
                <td><?=date('Y-m-d',strtotime($order->date))?></td>
                <td><?=$order->status_name?></td>
                <td align="center">
                    <a href="<?=base_url('index.php/master_order/konfirmasi/'.$order->order_id)?>" class="btn btn-success" style="width: 120px;">Konfirmasi</a>
                </td>
            </tr>
		    <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Order</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_order/order_update')?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="order_id" id="order_id">
          <table>
          <tr>
              <td>Account Name</td><td><input required disabled type="text" name="account" id="account" class="form-control"></td>
            </tr>
            <tr>
              <td>Product Name</td><td><input type="text" required disabled name="product" id="product" class="form-control"></td>
            </tr>
            <tr>
              <td>Payment Name</td><td><input type="text" required disabled name="payment" id="payment" class="form-control"></td>
            </tr>
            <tr>
              <td>Amount</td><td><input type="number" required disabled name="amount" id="amount" class="form-control"></td>
            </tr>
            <tr>
              <td>Qty</td><td><input type="number" required disabled name="qty" id="qty" class="form-control"></td>
            </tr>
            <tr>
              <td>Order Date</td><td><input type="text" required disabled name="order_date" id="order_date" class="form-control"></td>
            </tr>
            <tr>
            <tr>
              <td>Status</td>
              <td>
                <select name="status" required id="status" class="form-control">
      				<option></option>
      				<?php foreach ($status as $r): ?>
      					<option value="<?=$r->global_variable_id?>">
      						<?=$r->description ?></option>
      				<?php endforeach ?>
                </select>
              </td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="edit" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $(".datatable").dataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  });
</script>

<script>
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/master_order/edit_order/"+a, 
       dataType:"json",
       success:function(data){
        $("#order_id").val(data.order_id);
        $("#account").val(data.user_id);
        $("#product").val(data.product_id);
        $("#payment").val(data.payment_id);
        $("#amount").val(data.amount);
        $("#qty").val(data.output);
        $("#order_date").val(data.order_date);
        $("#status").val(data.status_id);
        $("#is_deleted").val(data.is_deleted);
      }
      });
    }
</script>