<form class="modal-content animate" action="<?=base_url('index.php/dashboard/proses_reset')?>" method="post">
			<div class="register-login-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="login-form">
                                <?php 
                                if($this->session->flashdata('pesan')!=null){
                                echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";}?>
								<h2>Reset Login</h2>
									<div class="group-input">
										<label for="username">Username *</label>
										<input type="text" name="resetUsername" style="padding: 12px 20px;">
									</div>
									<div class="group-input">
										<label for="pass">New Password *</label>
										<input type="password" name="resetPassword" style="padding: 12px 20px;">
									</div>
									<input type="submit" class="site-btn login-btn" name="reset" value="Sign In">
								<div class="switch-login">
									<a href="<?=base_url('index.php/dashboard/register')?>" class="or-login">Or Create An Account</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>