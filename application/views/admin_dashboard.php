<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $judul ?></title>

<!-- Custom fonts for this template -->
<link href="<?=base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?=base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

<!-- Custom styles for this page -->
<link href="<?=base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<link href="<?=base_url()?>asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Jquery Core Js -->
<script src="<?=base_url()?>asset/plugins/jquery/jquery.min.js"></script>
        <!-- Jquery DataTable Plugin Js -->
<script src="<?=base_url()?>asset/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<style>
  .dataTables_wrapper .dt-buttons {
    float: left; }
    .dataTables_wrapper .dt-buttons a.dt-button {
      background-color: #607D8B;
      color: #fff;
      padding: 7px 12px;
      margin-right: 5px;
      text-decoration: none;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.12);
      -webkit-border-radius: 2px;
      -moz-border-radius: 2px;
      -ms-border-radius: 2px;
      border-radius: 2px;
      border: none;
      font-size: 13px;
	  outline: none; }
	  
.dataTables_wrapper .dt-buttons a.dt-button:active {
opacity: 0.8; }
div.dataTables_wrapper div.dataTables_filter {
	text-align: right;
}
div.dataTables_wrapper div.dataTables_filter label {
	font-weight: normal;
	white-space: nowrap;
	text-align: left;
}
div.dataTables_wrapper div.dataTables_filter input {
	margin-left: 0.5em;
	display: inline-block;
	width: auto;
}
.dataTables_wrapper {
    position: relative;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
}
.pagination .disabled a,
.pagination .disabled a:hover,
.pagination .disabled a:focus,
.pagination .disabled a:active {
  color: #bbb; }

.pagination li.active a {
  background-color: #2196F3; }

.pagination li {
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  -ms-border-radius: 0;
  border-radius: 0; }
  .pagination li a:focus,
  .pagination li a:active {
    background-color: transparent;
    color: #555; }

.pagination > li > a {
  border: none;
  font-weight: bold;
  color: #555; }

.pagination > li:first-child > a,
.pagination > li:last-child > a {
  width: auto;
  height: 32px;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  -ms-border-radius: 0;
  border-radius: 0; }
  .pagination > li:first-child > a .material-icons,
  .pagination > li:last-child > a .material-icons {
    position: relative;
    bottom: 2px; }

.pagination-sm > li:first-child > a,
.pagination-sm > li:last-child > a {
  width: 28px;
  height: 28px; }
  .pagination-sm > li:first-child > a .material-icons,
  .pagination-sm > li:last-child > a .material-icons {
    position: relative;
    top: -1px;
    left: -6px;
    font-size: 20px; }

.pagination-lg > li:first-child > a,
.pagination-lg > li:last-child > a {
  width: 44px;
  height: 44px; }
  .pagination-lg > li:first-child > a .material-icons,
  .pagination-lg > li:last-child > a .material-icons {
    font-size: 30px;
    position: relative;
    top: -3px;
    left: -10px; }
    .pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px;
}
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.pagination-lg > li > a,
.pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.pagination-lg > li:first-child > a,
.pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.pagination-lg > li:last-child > a,
.pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.pagination-sm > li > a,
.pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination-sm > li:first-child > a,
.pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.pagination-sm > li:last-child > a,
.pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
</style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Role : Admin
      </div>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_account')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Account</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_cart')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Cart</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_order')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Order</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_list_product')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Product</span></a>
        </li>
        <!-- <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/pm')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Detail Product</span></a>
        </li> -->
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_role')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Role</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_color')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Color</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_status')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Status</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?=base_url('index.php/master_payment')?>">
        <i class="fas fa-fw fa-chart-area"></i>
            <span>Master Payment</span></a>
        </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- <div class="topbar-divider d-none d-sm-block"></div> -->

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="<?=base_url()?>asset/#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $this->session->userdata('fullname'); ?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?=base_url()?>index.php/admin/logout" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <?php 
            $this->load->view($konten);
        ?>

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?=base_url()?>asset/#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?=base_url()?>index.php/admin/logout">Logout</a>
        </div>
      </div>
    </div>
  </div>


<!-- Bootstrap Core Js -->
<script src="<?=base_url()?>asset/plugins/bootstrap/js/bootstrap.js"></script>
<!-- Slimscroll Plugin Js -->
<script src="<?=base_url()?>asset/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?=base_url()?>asset/plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?=base_url()?>asset/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="<?=base_url()?>asset/plugins/raphael/raphael.min.js"></script>
<script src="<?=base_url()?>asset/plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="<?=base_url()?>asset/plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?=base_url()?>asset/plugins/flot-charts/jquery.flot.js"></script>
<script src="<?=base_url()?>asset/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="<?=base_url()?>asset/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="<?=base_url()?>asset/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="<?=base_url()?>asset/plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?=base_url()?>asset/plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- Custom Js -->
<script src="<?=base_url()?>asset/js/admin.js"></script>
<script src="<?=base_url()?>asset/js/pages/index.js"></script>

<!-- Demo Js -->
<script src="<?=base_url()?>asset/js/demo.js"></script>

<!-- Bootstrap core JavaScript-->
<script src="<?=base_url()?>asset/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?=base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?=base_url()?>asset/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<!-- <script src="<?=base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script> -->
<script src="<?=base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?=base_url()?>asset/js/demo/datatables-demo.js"></script>

</body>

</html>
