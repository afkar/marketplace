<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $judul ?></title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?=base_url()?>asset/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/slicknav.min.css" type="text/css">
	<link rel="stylesheet" href="<?=base_url()?>asset/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/formLogin.css" type="text/css">
    <style>
        .container1{
            width:100%;
            padding-right:15px;
            padding-left:15px;
            margin-right:10px;
            margin-left:10px;
            margin: 10px;
            text-align: end;
        }
    </style>
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="nav-item" style="border-bottom: inset;">
            <div class="container1">
                <div class="nav-depart" align="left" style="/*margin-right: 25%;*/">
                    <div class="depart-btn" style="background: #ffffff;">
						<div class="logo">
							<a href="<?=base_url('index.php/dashboard')?>">
                            <h5 style="font-weight: bold; color: black;">EDITOR'S <p style="font-weight: bold; color: red;">DESK</p></h5>
							</a>
						</div>
                    </div>
				</div>
                <nav class="nav-menu mobile-menu" align="right">
                    <ul>
                        <li class="<?= $aktip1?>"><a href="<?=base_url('index.php/dashboard')?>">Home</a></li>
                        <li class="<?= $aktip2?>"><a href="<?=base_url('index.php/product')?>">Shop</a></li>
                        <li class="<?= $aktip4?>"><a href="<?=base_url('index.php/dashboard/portofolio')?>">Portofolio</a></li>
                        <li class="<?= $aktip3 ?>"><a href="<?=base_url('index.php/dashboard/about')?>">Contact US</a></li>
                    </ul>
                    <ul class="nav-right">
                        <li class="cart-icon">
                            <a href="<?=base_url()?>index.php/cart">
                                <i class="icon_bag_alt"></i>
                                <span>Cart</span>
                            </a>
                        </li>
                        <li class="heart-icon">
                            <a href="<?=base_url()?>index.php/dashboard/<?= $url; ?>">
                                <i class="fa fa-user"></i>
                                <span><?= $log ?></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
	<?php
		$this->load->view($konten);
		
	?>
	<!-- Hero Section End -->
	
	<!-- FormLogin Begin 
	<div id="id01" class="modal">
		<form class="modal-content animate" action="<?=base_url('index.php/dashboard/proses_login')?>" method="post">
			<div class="register-login-section spad">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="login-form">
								<h2>Login</h2>
									<div class="group-input">
										<label for="username">Username or email address *</label>
										<input type="text" id="username" name="username">
									</div>
									<div class="group-input">
										<label for="pass">Password *</label>
										<input type="text" id="pass" name="password">
									</div>
									<div class="group-input gi-check">
										<div class="gi-more">
											<label for="save-pass">
												Save Password
												<input type="checkbox" id="save-pass">
												<span class="checkmark"></span>
											</label>
											<a href="#" class="forget-pass">Forget your Password</a>
										</div>
									</div>
									<input type="submit" class="site-btn login-btn" name="login" value="Sign In">
								<div class="switch-login">
									<a href="<?=base_url('index.php/dashboard/register')?>" class="or-login">Or Create An Account</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	FormLogin End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <h5 style="font-weight: bold; color: white;">Editor Desk</h5>
                        </div>
                        <ul>
                            <li>Company name : Editor Desk</li>
                            <li>Company Address : Jl. Menuju rahmatnya</li>
                            <li>Owner : Anonymous</li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-lg-4 offset-lg-1">
                    <div class="footer-widget">
                        <h5>Information</h5>
                        <ul style="color: #ffffff;">
                            <li>Tag line (slogan)	Edukatif dan Menyenangkan</li>
                            <li>Vision & Mision 	Menjadi pionir mainan edukatif dengan produk puzzle</li>
                            <li>1. Melakukan inovasi berkelanjutan</li>
                            <li>2. Menjaga kualitas prouk</li>
                            <li>3.  Mengedukasi manfaat puzzle house sebagai alat edukasi kepada masyarakat</li>
                        </ul>
                    </div>
                </div> -->
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?=base_url()?>asset/js/jquery-3.3.1.min.js"></script>
    <script src="<?=base_url()?>asset/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery-ui.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery.countdown.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery.nice-select.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery.zoom.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery.dd.min.js"></script>
    <script src="<?=base_url()?>asset/js/jquery.slicknav.js"></script>
    <script src="<?=base_url()?>asset/js/owl.carousel.min.js"></script>
    <script src="<?=base_url()?>asset/js/main.js"></script>

	<!-- <script>
	// Get the modal
	var modal = document.getElementById('id01');

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
	</script> -->
</body>

</html>