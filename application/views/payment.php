<section class="checkout-section spad">
    <div class="container">
        <form action="<?=base_url('index.php/checkout/pay')?>" class="checkout-form">
            <div class="row">
                <div class="col-lg-6">
                    <div class="place-order">
                        <h4>Payment</h4>
                        <div class="order-total">
                            <ul class="order-table">
                                <li class="fw-normal">Account Name : <span><?= $dataOrder->account_name ?></span></li>
                                <li class="fw-normal">Payment <span><?= $dataOrder->payment_name ?></span></li>
                                <li class="fw-normal">Number<span><?= $dataOrder->account_number ?></span></li>
                            </ul>
                            <div class="payment-check">
                            <ul class="order-table">
                                <li class="fw-normal">Total bayar: <span><?= number_format($dataOrder->amount) ?></span></li>
                            </ul>
                            </div>
                            <div class="order-btn">
                                <button type="submit" class="site-btn place-btn">Pay</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>