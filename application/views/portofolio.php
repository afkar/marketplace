<!-- Instagram Section Begin -->
<div class="instagram-photo" style="white-space: pre-line;">
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/comp_desk.png' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/corner_desk.png' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/simple_desk.png' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/maxresdefault.jpg' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/corner_desk.png' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
    <div class="insta-item set-bg" data-setbg="<?=base_url('asset/gambar_product/maxresdefault.jpg' )?>">
        <div class="inside-text">
            <i class="ti-instagram"></i>
            <h5><a href="">colorlib_Collection</a></h5>
        </div>
    </div>
</div>
<!-- Instagram Section End -->