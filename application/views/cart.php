<!-- Shopping Cart Section Begin -->
<section class="shopping-cart spad">
        <div class="container">
            <?php           
                if($this->session->flashdata('pesan')!=null){
                    echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";
                }
            ?>
            <form action="<?=base_url('index.php/cart/simpan')?>" method="post">
                <div class="row">
                        <div class="col-lg-12">
                            <div class="cart-table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th class="p-name">Product Name</th>
                                            <th>Color</th>
                                            <!-- <th>Stock</th> -->
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                            <th>Action</i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dp as $items): ?>
                                        <tr>
                                            <input type="hidden" name="rowid[]" value="<?=$items['cart_id']?>">
                                            <input type="hidden" name="detail_product_id[]" value="<?=$items['detail_product_id']?>">
                                            <input type="hidden" name="user_id[]" value="<?=$items['user_id']?>">
                                            <input type="hidden" name="qty[]" value="<?=$items['output']?>">
                                            <td class="cart-pic first-row"><img src="<?=base_url('asset/gambar_product/'.$items['image']) ?>" width="50px" alt=""></td>
                                            <td class="cart-title first-row">
                                                <h5><?= $items['product_name'] ?></h5>
                                            </td>
                                            <td class="p-price first-row"><?= $items['color_name'] ?></td>
                                            <td class="p-price first-row"><?= number_format($items['price']) ?></td>
                                            <td class="p-price first-row"><?= $items['output'] ?></td>                                            
                                            <td class="total-price first-row"><?= number_format($items['price'] * $items['output']) ?></td>
                                            <td class="close-td first-row"><a href="<?=base_url('index.php/cart/hapus_cart/'.$items['cart_id'])?>"><i class="ti-close"></i></a></td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <!-- <div class="proceed-checkout">
                                        <input type="submit" name="update" class="proceed-btn" value="Update cart">
                                    </div> -->
                                </div>
                                <div class="col-lg-4 offset-lg-4">
                                    <div class="proceed-checkout">
                                        <ul>
                                            <li class="cart-total">Total <span><?= number_format($total) ?></span></li>
                                        </ul>
                                        <br>
                                        <input type="submit" class="proceed-btn" name="bayar" value="PROCEED TO CHECK OUT">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </form>
        </div>
    </section>
    <!-- Shopping Cart Section End -->