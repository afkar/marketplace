<div class="register-login-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="register-form">
                <?php 
                if($this->session->flashdata('pesan')!=null){
                echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";}?>
                    <h2>Register</h2>
                    <form action="<?=base_url('index.php/dashboard/simpan')?>" method="post">
                        <div class="group-input">
                            <label for="fullname">Fullname *</label>
                            <input type="text" style="border-radius: 10px;" name="fullname">
                        </div>
                        <div class="group-input">
                            <label for="email">Email Address *</label>
                            <input type="email" style="border-radius: 10px;" name="email">
                        </div>
                        <div class="group-input">
                            <label for="username">Username *</label>
                            <input type="text" style="border-radius: 10px;" name="username">
                        </div>
                        <div class="group-input">
                            <label for="pass">Password *</label>
                            <input type="password" style="border-radius: 10px;" name="password">
                        </div>
                        <div class="group-input">
                            <label for="con-pass">Address</label>
                            <input type="text" style="border-radius: 10px;" name=address>
                        </div>
                        <div class="group-input">
                            <label for="con-pass">Phone</label>
                            <input type="number" style="border-radius: 10px;" name="phone">
                        </div>
                        <input type="submit" class="site-btn register-btn" style="border-radius: 10px;" value="REGISTER" name="submit">
                    </form>
                    <!-- <div class="switch-login">
                        <a href="./login.html" class="or-login">Or Login</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>