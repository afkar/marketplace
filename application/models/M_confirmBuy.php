<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_ConfirmBuy extends CI_Model {
        
        // Insert to Cart
        public function insetCart()
        {            
                     
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'product_id' => $this->session->userdata('product_id'),                                
                'output' => $this->session->userdata('qty'),
                'status_id' => 1,
                'is_deleted' => 0
            );
            $this->session->set_userdata($data);
            
            $this->db->insert('cart', $data);
            
            if($this->db->affected_rows()>0){
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function postOrderDetail($order_id)
        {
            for($i=0;$i<count($this->input->post('rowid'));$i++){
                $data[] = array(
                    'order_id' => $order_id,
                    'product_id' => $this->input->post('product_id')[$i],                               
                    'output' => $this->input->post('qty')[$i],
                    'is_deleted' => 0
                );
            }
            
            $this->db->insert_batch('detail_order', $data);
            
            if($this->db->affected_rows()>0){
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function prosesOrder()
        {

            $data1 = array(
                'status_id' => 3
            );
            
            $this->db
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->where('is_deleted', 0)
                        ->update('cart', $data1);

            date_default_timezone_set('Asia/Jakarta');
            
            $data = array(
                'user_id' => $this->session->userdata('user_id'),
                'amount' => $this->input->post('amount'),
                'date' => date('Y-m-d H:i:s'),
                'payment_id' => $this->input->post('payment'),
                'status_id' => 3,
                'is_deleted' => 0
            );
            $this->session->set_userdata($data);
            
            $this->db->insert('order', $data);
            $order_id = $this->db->insert_id();
            return $order_id;
            // if($this->db->affected_rows()>0){
            //     return TRUE;
            // } else {
            //     return FALSE;
            // }
        }

        function addDetailOrder($data){
            $this->db->insert('detail_order', $data);
            return $this->db->insert_id();
        }

        public function konfirmasi_order($id)
	    {
            $object=array(
            'status_id'=> 4,
            );
            return $this->db->where('order_id',$id)->update('order', $object);
            //  $this->db->where('order_id',$id)->update('detail_order', $object);
    	}

        public function get_order()
        {
            return $this->db
                        ->join('user','user.user_id=order.user_id')
                        ->join('payment','payment.payment_id=order.payment_id')
                        ->join('status','status.status_id=order.status_id')
                        ->where('date', $this->session->userdata('date'))
                        ->get('order')
                        ->row();
            
        }
    
    }
    
    /* End of file ConfirmBuy.php */
    
?>