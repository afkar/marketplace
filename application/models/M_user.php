<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

    /**
     * Function for Register
    */
    public function register($data) {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

	public function masuk()
    {
		$email=$this->input->post('email');
		$fullname=$this->input->post('fullname');
		$address=$this->input->post('address');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$phone=$this->input->post('phone');
		$data_simpan=array(
			'email'=>$email,
			'fullname'=>$fullname,
			'address'=>$address,
			'username'=>$username,
			'password'=>$password,
			'phone'=>$phone,
			'role_id'=>2
			);
		$this->db->insert('user',$data_simpan);
		if($this->db->affected_rows()>0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function resetPW()
	{
		$object=array(
		'password'=>$this->input->post('resetPassword')
		);
		return $this->db
			->where('username',$this->input->post('resetUsername'))
			->update('user', $object);
	}

	public function get_login()
	{
		$password = $this->input->post('password');
		return $this->db
				->where('username',$this->input->post('username'))
				->where('password',$this->input->post('password'))
				->where('is_deleted',0)
				->get('user');
				// ->where('password',md5($password))
	}

	public function get_loginAdmin()
	{
		$password = $this->input->post('password');
		return $this->db
				->where('username',$this->input->post('usernameAdmin'))
				->where('password',$this->input->post('passwordAdmin'))
				->where('is_deleted',0)
				->get('user');
				// ->where('password',md5($password))
	}

	public function get_account()
	{
		// ->query("SELECT * FROM user a JOIN role b on b.role_id equals a.role_id where is_deleted == 0")-
		return $this->db
					->join('role','role.role_id=user.role_id')
					->where('user.is_deleted', 0)
					->get('user')
					->result();
	}

	public function get_roles()
	{
		return $this->db->where('is_deleted','0')->get('role')->result();
	}

	public function get_role()
	{
		return $this->db
				->where('role_id',$this->session->userdata('role_idAdmin'))
				->get('role');
	}

	public function detail_profil($user_id)
	{
		return $this->db
					->where('user_id',$user_id)
					->get('user')
					->row();
	}

	public function update_account()
	{
		$object=array(
		'fullname'=>$this->input->post('fullname'),
		'address'=>$this->input->post('address'),
		'phone'=>$this->input->post('phone'),
		'email'=>$this->input->post('email'),
		'username'=>$this->input->post('username'),
		'password'=>$this->input->post('password'),
		'role_id'=>$this->input->post('role'),
		'is_deleted'=>$this->input->post('is_deleted')
		);
		return $this->db
					->where('user_id',$this->input->post('user_id'))
					->update('user', $object);
	}

	public function simpan_user()
	{
		$email=$this->input->post('email');
		$fullname=$this->input->post('fullname');
		$address=$this->input->post('address');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$phone=$this->input->post('phone');
		$role=$this->input->post('role');
		$data_simpan=array(
			'email'=>$email,
			'fullname'=>$fullname,
			'address'=>$address,
			'username'=>$username,
			'password'=>$password,
			'phone'=>$phone,
			'role_id'=>$role,
			'is_deleted'=>0
			);
		$this->db->insert('user',$data_simpan);
		$this->session->set_flashdata('pesan', 'Sukses menambahkan');
	}

	// Delete User
	public function delete_user($id)
	{
		$object=array(
		'is_deleted'=>1,
		);
		return $this->db
					->where('user_id',$id)
					->update('user', $object);
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */