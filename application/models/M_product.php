<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends CI_Model {

	public function getListProduct()
	{
		$result=$this->db
					 ->join('categories', 'categories.categories_id=product.categories_id')
					 ->where('product.is_deleted',0)
					 ->get('product')->result();
		return $result;
	}

	public function getListProductByCat($cat)
	{
		$result=$this->db
					->where('categories_id',$cat)
					 ->where('is_deleted',0)
					 ->get('product')->result();
		return $result;
	}

	public function getListProduct1()
	{
		$result=$this->db
					->join('product', 'product.product_id=detail_product.product_id')
					->join('color', 'color.color_id=detail_product.color_id')
					->where('detail_product.is_deleted',0)
					->get('detail_product')->result();
		return $result;
	}

	public function getListColor()
	{
		return $this->db->where('is_deleted', 0)->get('color')->result();
		
	}

	public function getProduct2()
	{
		$result=$this->db
					 ->join('categories', 'categories.categories_id=product.categories_id')
					 ->where('product.is_deleted',0)
					 ->limit(2)
					 ->get('product')->result();
		return $result;
	}

    /**
     * Function for Get Product By Id
    */
    public function getProductById($product_id)
    {   
		$result=$this->db
					->join('categories', 'categories.categories_id=product.categories_id')
					->where('product.is_deleted',0)
					->where('product_id',$product_id)
					->get('product')
					->row();
		return $result;
	}

	public function getDetailProductById($product_id)
	{
		return $this->db
					->join('product','product.product_id=detail_product.product_id')
					->join('color','color.color_id=detail_product.color_id')
					->where('detail_product.detail_product_id',$product_id)
					->get('detail_product')
					->row();
		
	}

	public function getDetailProductById1($product_id)
	{
		return $this->db
					->join('product','product.product_id=detail_product.product_id')
					->join('color','color.color_id=detail_product.color_id')
					->where('detail_product.product_id',$product_id)
					->where('detail_product.is_deleted',0)
					->get('detail_product')
					->result();
		
	}

	public function getDetailProductByColor($product_id)
	{
		return $this->db
					->join('product','product.product_id=detail_product.product_id')
					->join('color','color.color_id=detail_product.color_id')
					->where([
						'detail_product.product_id' => $product_id,
						'detail_product.color_id' => $this->input->post('color')
						])
					->get('detail_product')
					->row();
		
	}

	public function simpan_detail()
	{
		$object=array(
			'product_id'=>$this->input->post('product'),
			'color_id'=>$this->input->post('color'),
			'stock'=>$this->input->post('stock'),
			'is_deleted'=>0
		);
		$this->db->insert('detail_product', $object);
		
	}
	
	public function simpan_product($nama_file)
	{
		if($nama_file==""){
			$object=array(
                'product_name'=>$this->input->post('product_name'),
				'categories_id'=>$this->input->post('categories'),
                'price'=>$this->input->post('price'),
                'description'=>$this->input->post('deskripsi'),
                'is_deleted'=>0
			);
		} else{
			$object=array(
                'product_name'=>$this->input->post('product_name'),
				'categories_id'=>$this->input->post('categories'),
                'price'=>$this->input->post('price'),
                'description'=>$this->input->post('deskripsi'),
                'is_deleted'=>0,
                'image'=>$nama_file
			);
		}		
		$this->db->insert('product', $object);		
		return $this->db->insert_id();
	}

	public function simpan_detail_product(){
		$data = array(
			'product_id' => $this->input->post('product'),
			'color_id' => $this->input->post('color'),
			'stock' => $this->input->post('stock'),
			'is_deleted'=>0
		);
		return $this->db->insert('detail_product', $data);
	}

	public function product_update_dengan_foto($nama_foto='')
	{
		$object=array(
			'product_name'=>$this->input->post('product_name'),
			'categories_id'=>$this->input->post('categories'),
			'price'=>$this->input->post('price'),
			'description'=>$this->input->post('deskripsi'),
			// 'is_deleted'=>$this->input->post('is_deleted'),
			'image'=>$nama_foto
		);
		
		$this->db->where('product_id',$this->input->post('product_id'))
					->update('product', $object);	
		$data = array(
			'color_id' => $this->input->post('color_id'),
			'stock' => $this->input->post('stock'),
			'is_deleted'=>$this->input->post('is_deleted')
		);
		return $this->db->where('detail_product_id',$this->input->post('detail_product_id'))
		->update('detail_product', $data);
	}

	public function product_update_no_foto()
	{
		$object=array(
			'product_name'=>$this->input->post('product_name'),
			'categories_id'=>$this->input->post('categories'),
			'price'=>$this->input->post('price'),
			'description'=>$this->input->post('deskripsi')
			// 'is_deleted'=>$this->input->post('is_deleted')
		);
		$this->db->where('product_id',$this->input->post('product_id'))
					->update('product', $object);	
		$data = array(
			'color_id' => $this->input->post('color_id'),
			'stock' => $this->input->post('stock'),
			'is_deleted'=>$this->input->post('is_deleted')
		);
		return $this->db->where('detail_product_id',$this->input->post('detail_product_id'))
		->update('detail_product', $data);
	}

	public function update_detail_product()
	{
		$data = array(
			'stock'=>$this->input->post('stock'),
			'color_id'=>$this->input->post('color')
		);
		return $this->db
					->where('detail_product_id',$this->input->post('detail_product_id'))
					->update('detail_product', $data);
	}

	public function update_stock_product($id,$stock)
	{
		$data = array(
			'stock'=>$stock
		);
		return $this->db
					->where('detail_product_id',$id)
					->update('detail_product', $data);
	}

	// Delete Product From Product
	public function hapus_product($id)
	{
		$object=array(
		'is_deleted'=>1,
		);
		return $this->db
					->where('product_id',$id)
					->update('product', $object);
	}
		

	// Delete Detail Product
	public function delete_detail_product($id)
	{
		$object=array(
		'is_deleted'=>1,
		);
		return $this->db
					->where('detail_product_id',$id)
					->update('detail_product', $object);
	}

}

/* End of file ModelName.php */

?>