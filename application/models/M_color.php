<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Color extends CI_Model {

    public function get_color()
    {
        return $this->db
            ->where('is_deleted',0)
            ->get('color')
            ->result();
    }

	public function simpan_color()
	{
		$color_name=$this->input->post('color_name');
		$data_simpan=array(
			'color_name'=>$color_name,
			'is_deleted'=>0
			);
		$this->db->insert('color',$data_simpan);
	}

	public function detail_color($id)
	{
		return $this->db
					->where('color_id',$id)
					->get('color')
					->row();
	}

	public function update_color()
	{
		$object=array(
		'color_name'=>$this->input->post('color_name'),
		);
		return $this->db
					->where('color_id',$this->input->post('color_id'))
					->update('color', $object);
	}

	// Delete Color
    public function delete_color($id)
    {
        $object=array(
        'is_deleted'=>1,
        );
        return $this->db
                    ->where('color_id',$id)
                    ->update('color', $object);
    }

}

/* End of file M_Color.php */

?>