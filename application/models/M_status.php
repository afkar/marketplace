<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Status extends CI_Model {

    public function get_status()
    {
        return $this->db
            ->where('is_deleted', 0)
            ->get('status')
            ->result();
    }

	public function simpan_status()
	{
		$status_name=$this->input->post('status_name');
		$data_simpan=array(
			'status_name'=>$status_name,
			'is_deleted'=>0
			);
		$this->db->insert('status',$data_simpan);
	}

	public function detail_status($id)
	{
		return $this->db
					->where('status_id',$id)
					->get('status')
					->row();
	}

	public function update_status()
	{
		$object=array(
		'status_name'=>$this->input->post('status_name'),
		);
		return $this->db
					->where('status_id',$this->input->post('status_id'))
					->update('status', $object);
	}

	// Delete Status
    public function delete_status($id)
    {
        $object=array(
        'is_deleted'=>1,
        );
        return $this->db
                    ->where('status_id',$id)
                    ->update('status', $object);
    }

}

/* End of file M_Status.php */

?>