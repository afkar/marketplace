<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Payment extends CI_Model {

    public function get_payment()
    {
		return $this->db
			->where('is_deleted',0)
            ->get('payment')
            ->result();
    }

	public function simpan_payment()
	{
		$payment_name=$this->input->post('payment_name');
		$account_number=$this->input->post('account_number');
		$account_name=$this->input->post('account_name');
		$data_simpan=array(
			'payment_name'=>$payment_name,
			'account_number'=>$account_number,
			'account_name'=>$account_name,
			'is_deleted'=>0
			);
		$this->db->insert('payment',$data_simpan);
	}

	public function detail_payment($id)
	{
		return $this->db
					->where('payment_id',$id)
					->get('payment')
					->row();
	}

	public function update_payment()
	{
		$object=array(
		'payment_name'=>$this->input->post('payment_name'),
        'account_number'=>$this->input->post('account_number'),
        'account_name'=>$this->input->post('account_name')
		);
		return $this->db
					->where('payment_id',$this->input->post('payment_id'))
					->update('payment', $object);
	}

	// Delete Payment
	public function delete_payment($id)
	{
		$object=array(
		'is_deleted'=>1,
		);
		return $this->db
					->where('payment_id',$id)
					->update('payment', $object);
	}

}

/* End of file M_Payment.php */

?>