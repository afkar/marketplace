<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Order extends CI_Model {

	// Get List Order
    public function get_order()
    {
        return $this->db
            ->join('user','user.user_id=order.user_id')
            ->join('payment','payment.payment_id=order.payment_id')
            ->join('status','status.status_id=order.status_id')
            ->get('order')
            ->result();
    }
    public function get_order1()
    {
        return $this->db
            ->join('order','order.order_id=detail_order.order_id')
            ->join('cart','cart.cart_id=detail_order.cart_id')
            ->join('user','user.user_id=order.user_id')
            ->join('payment','payment.payment_id=order.payment_id')
			->join('status','status.status_id=order.status_id')
			->group_by('detail_order.order_id')
            ->get('detail_order')
            ->result();
    }

	public function get_status()
	{
		return $this->db
					->get('status')->result();
	}
	
	// Get List Order By Status
	public function get_order_byStatus($status_id)
    {
        return $this->db
            ->join('user','user.user_id=order.user_id')
            ->join('payment','payment.payment_id=order.payment_id')
			->join('status','status.status_id=order.status_id')
			->where('order.status_id',$status_id)
			->where('user.is_deleted', 0)
			->where('payment.is_deleted', 0)
			->where('status.is_deleted', 0)
			->where('order.is_deleted', 0)
            ->get('order')
            ->result();
    }

	// Get Detail Order
	public function detail_order($order_id)
	{		
		return $this->db
					->join('order','order.order_id = detail_order.order_id')
					->join('user','user.user_id=order.user_id')
            		->join('payment','payment.payment_id=order.payment_id')
					->join('status','status.status_id=order.status_id')
					->where('detail_order.order_id',$order_id)
					->where('user.is_deleted', 0)
					->where('payment.is_deleted', 0)
					->where('status.is_deleted', 0)
					->where('order.is_deleted', 0)
					->get('detail_order')
					->row();
	}

	public function get_detail($order_id)
	{		
		return $this->db
					->join('order','order.order_id = detail_order.order_id')
					->join('user','user.user_id=order.user_id')
            		->join('payment','payment.payment_id=order.payment_id')
					->join('status','status.status_id=order.status_id')
					->where('detail_order.order_id',$order_id)
					->where('user.is_deleted', 0)
					->where('payment.is_deleted', 0)
					->where('status.is_deleted', 0)
					->where('order.is_deleted', 0)
					->get('detail_order')
					->result();
	}
	

	public function update_order()
	{
		$object=array(
		'status_id'=>$this->input->post('status'),
		);
		return $this->db
					->where('order_id',$this->input->post('order_id'))
					->update('order', $object);
	}

	public function hapus_order($id)
	{
		$object=array(
		'is_deleted'=> 1,
		);
		return $this->db->where('order_id',$id)->update('order', $object);
	}

	public function konfirmasi_order($id)
	{
		$object=array(
		'status_id'=> 4,
		);
		return $this->db->where('order_id',$id)->update('order', $object);
	}

}

/* End of file M_Order.php */

?>