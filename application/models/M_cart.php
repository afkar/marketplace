<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_Cart extends CI_Model {
        
        // Insert to Cart
        public function updateToCheckout()
        {
            
            // for($i=0;$i<count($this->input->post('rowid'));$i++){
            //     $data[] = array(
            //         'user_id' => $this->session->userdata('user_id'),
            //         'product_id' => $this->input->post('product_id')[$i],
            //         'status_id' => 2,
            //         'detail_product_id' => $this->input->post('detail_product_id')[$i],
            //         'output' => $this->input->post('qty')[$i],
            //         'is_deleted' => 0
            //     );
            // }
            $data = array(
                'status_id' => 2
            );
            
            return $this->db
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->where('status_id', 1)
                        ->where('is_deleted', 0)
                        ->update('cart', $data);
            
            // if($this->db->affected_rows()>0){
            //     return TRUE;
            // } else {
            //     return FALSE;
            // }
        }

        public function insertCart($data){     
            // print_r($data); 
            $this->db->insert('cart',$data);
            return $this->db->insert_id();    
        }

        // Get cart by user when status 1(in_cart)
        public function get_cart_byUserId()
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        ->join('product','product.product_id=cart.product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->join('detail_product','detail_product.detail_product_id=cart.status_id')
                        ->join('color','color.color_id=detail_product.color_id')
                        ->where('cart.user_id', $this->session->userdata('user_id'))
                        ->where('cart.status_id', 1)
                        ->where('cart.is_deleted', 0)
                        ->get('cart')
                        ->result();
            
        }
        public function get_checkout_byUserId()
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        ->join('product','product.product_id=cart.product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->join('detail_product','detail_product.detail_product_id=cart.status_id')
                        ->join('color','color.color_id=detail_product.color_id')
                        ->where('cart.user_id', $this->session->userdata('user_id'))
                        ->where('cart.status_id', 2)
                        ->where('cart.is_deleted', 0)
                        ->get('cart')
                        ->result();
            
        }

        public function get_payment_byUserId()
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        ->join('product','product.product_id=cart.product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->join('detail_product','detail_product.detail_product_id=cart.status_id')
                        ->join('color','color.color_id=detail_product.color_id')
                        ->where('cart.user_id', $this->session->userdata('user_id'))
                        ->where('cart.status_id', 3)
                        ->where('cart.is_deleted', 0)
                        ->get('cart')
                        ->result();
            
        }

        // Get List Cart when status 1(in_cart)
        public function get_cart()
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        ->join('product','product.product_id=cart.product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->where('cart.status_id', 1)
                        ->where('cart.is_deleted', 0)
                        ->where('user.is_deleted', 0)
                        ->where('product.is_deleted', 0)
                        ->where('status.is_deleted', 0)
                        ->get('cart')
                        ->row();
            
        }

        public function get_cart_byCartId($id)
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        // ->join('product','product.product_id=cart.product_id')
                        ->join('detail_product','detail_product.detail_product_id=cart.detail_product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->where('cart.cart_id', $id)
                        ->where('cart.is_deleted', 0)
                        ->where('user.is_deleted', 0)
                        // ->where('product.is_deleted', 0)
                        // ->where('detail_product.is_deleted', 0)
                        ->where('status.is_deleted', 0)
                        ->get('cart')
                        ->row();
            
        }

        public function get_list_cart()
        {
            return $this->db
                        ->join('user','user.user_id=cart.user_id')
                        ->join('product','product.product_id=cart.product_id')
                        ->join('status','status.status_id=cart.status_id')
                        ->join('detail_product','detail_product.detail_product_id=cart.detail_product_id')
                        ->join('color','color.color_id=detail_product.color_id')
                        ->where('cart.status_id', 1)
                        ->where('cart.is_deleted', 0)
                        ->where('user.is_deleted', 0)
                        ->where('product.is_deleted', 0)
                        ->where('status.is_deleted', 0)
                        ->get('cart')
                        ->result();
            
        }


        // Update Status Cart(Set to status_id 2 (in_checkout))
        public function update_order()
        {
            $object=array(
            'status_id'=>$this->input->post('status'),
            );
            return $this->db
                        ->where('cart_id',$this->input->post('cart_id'))
                        ->update('cart', $object);
        }

        public function complete_order($id)
        {
            $object=array(
            'status_id'=>4,
            );
            return $this->db
                        ->where('cart_id',$id)
                        ->update('cart', $object);
        }

        // Delete Cart
        public function delete_cart($id)
        {
            $object=array(
            'is_deleted'=>1,
            );
            return $this->db
                        ->where('cart_id',$id)
                        ->update('cart', $object);
        }
    
    }
    
    /* End of file ConfirmBuy.php */
    
?>