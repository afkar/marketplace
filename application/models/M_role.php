<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Role extends CI_Model {

    public function get_role()
    {
		return $this->db
			->where('is_deleted',0)
            ->get('role')
            ->result();
    }

	public function simpan_role()
	{
		$role_name=$this->input->post('role_name');
		$data_simpan=array(
			'role_name'=>$role_name,
			'is_deleted'=>0
			);
		$this->db->insert('role',$data_simpan);
	}

	public function detail_role($id)
	{
		return $this->db
					->where('role_id',$id)
					->get('role')
					->row();
	}

	public function update_role()
	{
		$object=array(
		'role_name'=>$this->input->post('role_name'),
		);
		return $this->db
					->where('role_id',$this->input->post('role_id'))
					->update('role', $object);
	}

	// Delete Role
	public function delete_role($id)
	{
		$object=array(
		'is_deleted'=>1,
		);
		return $this->db
					->where('role_id',$id)
					->update('role', $object);
	}

}

/* End of file M_Role.php */

?>