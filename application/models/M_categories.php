<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_categories extends CI_Model {
    
        public function get_categories()
        {
            return $this->db->where('is_deleted', 0)->get('categories')->result();
        }
    
    }
    
    /* End of file M_categories.php */
    
?>