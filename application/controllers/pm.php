<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Pm extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_product','product');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
            
        }
        
    
        public function index()
        {
			$data['dataProduct']=$this->product->getDetailProductById1($this->session->userdata('product_idnye'));
			$data['color']=$this->product->getListColor();
			$data['list_product']=$this->product->getListProduct();
            $data['konten']='data_product';
            $data['judul']='Data Detail Product';
            $this->load->view('admin_dashboard', $data);
        }

        public function edit_detail_product($id)
        {
            $data=$this->product->getDetailProductById($id);
            echo json_encode($data);
        }
    
		public function hapus($id_product='')
		{
			if($this->product->delete_detail_product($id_product)){
				$this->session->set_flashdata('pesan', 'Sukses Hapus product');
				redirect('pm','refresh');
			} else {
				$this->session->set_flashdata('pesan', 'Gagal Hapus product');
				redirect('pm','refresh');	
			}
		}
        
        public function product_update()
        {
            if($this->product->update_detail_product()){
                $this->session->set_flashdata('pesan', 'Sukses update');
                redirect('pm');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal update');
                redirect('pm');
            }
    
        }
        
        public function tambah()
        {
            if($this->input->post('simpan')){
                if($this->product->simpan_detail_product()){
                    $this->session->set_flashdata('pesan', 'sukses menambah');	
                } else {
                    $this->session->set_flashdata('pesan', 'gagal menambah');	
                }
                
                $array = array(
                    'product_idnye' => $this->input->post('product')
                );
                
                $this->session->set_userdata( $array );
                
                redirect('pm','refresh');	
            }    
        }
    
    }
    
    /* End of file Controllername.php */
    
?>