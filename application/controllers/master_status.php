<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Master_Status extends CI_Controller {
        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_status', 'status');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
        }
        
    
        public function index()
        {
			$data['dataStatus']=$this->status->get_status();
            $data['konten']='master_status';
            $data['judul']='Data Status';
            $this->load->view('admin_dashboard', $data);
        }

        public function tambah()
        {
            if($this->input->post('simpan')){
                if($this->status->simpan_status()){
                    $this->session->set_flashdata('pesan', 'Sukses menambahkan');
                    redirect('master_status','refresh');
                } else {
                    $this->session->set_flashdata('pesan', 'Gagal menambahkan');
                    redirect('master_status','refresh');
                }
            }
        }
    
        public function edit_status($id)
        {
            $data=$this->status->detail_status($id);
            echo json_encode($data);
        }
    
        public function status_update()
        {
            if($this->input->post('edit')){
                if($this->status->update_status()){
                $this->session->set_flashdata('pesan', 'Sukses Update');
                redirect('master_status','refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal Hapus');
                redirect('master_status','refresh');	
            }
            }
        }
    
        public function hapus($id)
        {
            if($this->status->delete_status($id)){
                $this->session->set_flashdata('pesan', 'Sukses menghapus');
                redirect('master_status','refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal menghapus');
                redirect('master_status','refresh');
            }
        }
    
    }
    
    /* End of file Master_Status.php */
    
?>