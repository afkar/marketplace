<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Cart extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('M_cart', 'mcart');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
        }
        
    
        public function index(){
            $detail_product=$this->mcart->get_cart_byUserId();            
            $dp = json_decode(json_encode($detail_product), true);                 
            $total = 0;
            for($i = 0 ; COUNT($detail_product) > $i; $i++)
                $total = $total + ($detail_product[$i]->price * $detail_product[$i]->output);
            
            
            $array = array(
                'totalnya' => $total
            );
            
            $this->session->set_userdata( $array );
            
            $data['total'] = $total;    
            $data['dp']=$dp;                     
            $data['konten']="cart";
            $data['judul']="Cart";
            $data['aktip1']="";
            $data['aktip2']="";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
            }
            $this->load->view('dashboard', $data);
        }

        public function hapus_cart($id)
        {                    
            $this->mcart->delete_cart($id);
            redirect('cart','refresh');
        }

        public function simpan()
        {
            if ($this->input->post('update')) {
            
                for($i=0;$i<count($this->input->post('rowid'));$i++){
                    if($this->input->post('qty')[$i] <= $this->input->post('stock')[$i]){
                        $data = array(
                            'rowid' => $this->input->post('rowid')[$i],
                            'qty'   => $this->input->post('qty')[$i]
                        ); 
                        $this->cart->update($data);
                    }else{
                        $this->session->set_flashdata('pesan', 'Jumlah pembelian tidak mencukupi stock yang ada');
                    }
                    
                }
                redirect('cart','refresh');		
            } elseif($this->input->post('bayar')){
                    $this->mcart->updateToCheckout();
                    $this->session->set_flashdata('pesan', 'Add Cart Done');
                    redirect('checkout','refresh');
                
            }
        }
    
    }
    
    /* End of file Controllername.php */
    
?>