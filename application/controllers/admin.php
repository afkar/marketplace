<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Admin extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('m_user', 'user');
            
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
        }
        
        public function index()
        {
            $data['konten']='home_admin';
            $data['judul']='Admin';
            $this->load->view('admin_dashboard', $data);
        }

        public function logout()
        {
            
            $this->session->sess_destroy();
            
            redirect('dashboard/login','refresh');
            
        }
    
    }
    
    /* End of file Admin.php */
    
?>