<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Master_Cart extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_cart', 'mCart');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
            
        }
        
    
        public function index()
        {
            $data['dataCart']=$this->mCart->get_list_cart();
            $data['konten']='master_cart';
            $data['judul']='Data Cart';
            $this->load->view('admin_dashboard', $data);
        }
    
    }
    
    /* End of file Master_Cart.php */
    
?>