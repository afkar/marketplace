<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Checkout extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_product', 'product');
            $this->load->model('m_user', 'account');
            $this->load->model('m_confirmBuy', 'confirm');
            $this->load->model('m_payment','payment');
            $this->load->model('m_cart','inCart');
            $this->load->model('m_order','order');
            $this->load->model('m_categories', 'cat');
            date_default_timezone_set('Asia/Jakarta');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
            
        }
        
        public function AddCart($product_id)
        {
            $detail_product = $this->product->getDetailProductByColor($product_id);
            if ($detail_product->stock >= $this->input->post('qty')) {
                
                $dataDB = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'product_id' => $detail_product->product_id,
                    'status_id' => 1,
                    'detail_product_id' => $detail_product->detail_product_id,
                    'output' => $this->input->post('qty'),
                    'is_deleted' => 0
                );                  
                $this->inCart->insertCart($dataDB);  
                
                $this->session->userdata('qtyProduct', $this->input->post('qty'));
                
                $this->session->set_flashdata('pesan', 'Sukses add Cart');
                redirect('product','refresh');
                
            }else{
                $this->session->set_flashdata('pesan', 'Stock tidak mencukupi dengan jumlah pembelian anda');
                redirect('product','refresh');
            }
        }
    
        public function confirmBuy()
        {
            if($this->input->post('pay')){
                $detail_product=$this->inCart->get_checkout_byUserId();                                                 
                $order_id = $this->confirm->prosesOrder();               
                for($i = 0 ; COUNT($detail_product) > $i; $i++){
                    $data = array(
                        'detail_order_id' => '',
                        'order_id' => $order_id,
                        'cart_id' => $detail_product[$i]->cart_id,
                        'is_deleted' => 0
                    );
                    $this->confirm->addDetailOrder($data);
                }
                $this->session->set_flashdata('pesan', 'Sukses Membeli');
                redirect('checkout/payment','refresh');
                // if($this->confirm->prosesOrder()){
                //     $this->session->set_flashdata('pesan', 'Sukses Membeli');
                //     redirect('checkout/payment','refresh');
                // } else {
                //     $this->session->set_flashdata('pesan', 'Gagal Membeli');
                //     redirect('product','refresh');
                // }
            }
        }
	
        public function pay()
        {       
            $order_id = $_SESSION['order_id'];
            $detail_order = $this->order->get_detail($order_id);            
            // print_r($detail_order);
            // exit();         
            $data['dataProduct'] = $this->product->getListProduct();
            $data['dataCategories'] = $this->cat->get_categories();
            $data['konten']="product";
            $data['judul']="My Product";
            $data['aktip1']="";
            $data['aktip2']="active";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
            }
            // $this->cart->destroy();     
            $this->confirm->konfirmasi_order($order_id); 
            for($i = 0 ; COUNT($detail_order) > $i; $i++){                
                $this->inCart->complete_order($detail_order[$i]->cart_id);
                $dt = $this->inCart->get_cart_byCartId($detail_order[$i]->cart_id);
                $totStock = $dt->stock - $dt->output;
                $this->product->update_stock_product($dt->detail_product_id,$totStock);                
            }      
            $this->session->set_flashdata('pesan', 'Sukses Membeli');
            $this->load->view('dashboard', $data);	
        }

        public function payment()
        {                    
            $data['dataOrder']=$this->confirm->get_order();
            $data['konten']="payment";
            $data['judul']="Payment";
            $data['aktip1']="";
            $data['aktip2']="";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
            }            
            $_SESSION['order_id'] = $data['dataOrder']->order_id;
            $this->load->view('dashboard', $data);	
        }

        public function index()
        {
            $detail_product=$this->inCart->get_checkout_byUserId();            
            $dp = json_decode(json_encode($detail_product), true);                 
            $total = 0;
            for($i = 0 ; COUNT($detail_product) > $i; $i++)
                $total = $total + ($detail_product[$i]->price * $detail_product[$i]->output);

            $data['total'] = $total;    
            $data['dp']=$dp;   

            $data['dataOrder']=$this->confirm->get_order();
            $data['dataPayment']=$this->payment->get_payment();
            $da = array(
                'qty' => $this->input->post('qty')
            );
            $this->session->set_userdata( $da );
            $data['dataAmount']=$this->session->userdata('ammount') * $this->input->post('qty');
            $data['aktip1']="";
            $data['aktip2']="active";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
                $data['konten']="check_out";
                $data['judul']="Checkout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
                $data['konten']="login";
                $data['judul']="Login";
                $this->session->set_flashdata('pesan', 'Silahkan Login Terlebih Dahulu');
            }
            $this->load->view('dashboard', $data);
        }
    
    }
    
    /* End of file Checkout.php */
        
?>