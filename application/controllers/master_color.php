<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Master_color extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_color', 'color');
            
            if($this->session->userdata('login')!=true){
                redirect(base_url('index.php/dashboard/login'),'refresh');
            }
            
        }
        
    
        public function index()
        {
			$data['dataColor']=$this->color->get_color();
            $data['konten']='master_color';
            $data['judul']='Data Color';
            $this->load->view('admin_dashboard', $data);
        }

        public function tambah()
        {
            if($this->input->post('simpan')){
                if($this->color->simpan_color()){
                    $this->session->set_flashdata('pesan', 'Sukses menambahkan');
                    redirect('master_color','refresh');
                } else {
                    $this->session->set_flashdata('pesan', 'Gagal menambahkan');
                    redirect('master_color','refresh');
                }
            }
        }
    
        public function edit_color($id)
        {
            $data=$this->color->detail_color($id);
            echo json_encode($data);
        }
    
        public function color_update()
        {
            if($this->input->post('edit')){
                if($this->color->update_color()){
                $this->session->set_flashdata('pesan', 'Sukses Update');
                redirect('master_color','refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal Hapus');
                redirect('master_color','refresh');	
            }
            }
        }
    
        public function hapus($id)
        {
            if($this->color->delete_color($id)){
                $this->session->set_flashdata('pesan', 'Sukses menghapus');
                redirect('master_color','refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal menghapus');
                redirect('master_color','refresh');
            }
        }
    
    }
    
    /* End of file Controllername.php */
    
?>