<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('admin');
	}

    /**
     * Function for Add Role
    */
    public function addRole()
    {
        $role_name = "Admin";        
        $data = array (
                    "role_id" => '',
                    "role_name" => $role_name,                    
                    "is_deleted" => 0   
                );
        $result = $this->admin->addRole($data);
        return $result;
    }

    /**
     * Function for Update Role
    */
    public function updateRole()
    {
        $role_id = 1;           
        $this->admin->updateRole($role_id);                                    

    }
    
    /**
     * Function for Delete Role
    */
    public function deleteRole()
    {                        
        $role_id = 3;         
        $this->admin->deleteRole($role_id);                                            
    }

    
}
?>